

package INF101.lab2;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.ArrayList;

public class Fridge implements IFridge {

    List<FridgeItem> FridgeItemContent = new ArrayList<FridgeItem>();
    int max_size = 20;

    @Override
    public int nItemsInFridge() {
        return FridgeItemContent.size();
    }

    @Override
    public int totalSize() {
        return max_size;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (FridgeItemContent.size() < 20) {
            FridgeItemContent.add(item);
            return true;
        } else {
            return false;
        }

    }

    @Override
    public void takeOut(FridgeItem item) {
        if (FridgeItemContent.contains(item)) {
            FridgeItemContent.remove(item);
        } else {
            throw new NoSuchElementException("This items does not exits, or you wrote the name wrong");
        }

    }

    @Override
    public void emptyFridge() {
        FridgeItemContent.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredFood = new ArrayList<FridgeItem>();
        for (int i = 0; i < FridgeItemContent.size(); i++) {
            if (FridgeItemContent.get(i).hasExpired()) {
                expiredFood.add(FridgeItemContent.get(i));
            }
        }
        for (int i = 0; i < expiredFood.size(); i++) {
            takeOut(FridgeItemContent.get(i));
        }
        return expiredFood;
    }

}
